import {UserData} from "./UserData";

class UserPermissionsClass {

    constructor() {
        this.roles = [
            {displayName: 'Administrator', role: 'ADMIN'},
            {displayName: 'Staff', role: 'STAFF'}
        ];

        this.ADMIN = this.roles[0].role;
        this.STAFF = this.roles[1].role;

        this.getRoles = this.getRoles.bind(this);
        this.canManageUsers = this.canManageUsers.bind(this);
    }

    getRoles() {
        return this.roles;
    }

    canManageUsers() {
        return (UserData.getCurrentRole() === this.ADMIN);
    }

}

export const UserPermissions = new UserPermissionsClass();