package com.prodigy.api.rest;

import com.prodigy.business.user.UserService;
import com.prodigy.business.user.domain.UserRepository;
import com.prodigy.business.user.dto.UserViewDto;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Log4j2
@RestController
@RequestMapping("api")
public class StaffController {

    private final UserService userService;

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    public StaffController(UserService userService, UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping("/users")
    List<UserViewDto> getUsers() {

        UserDetails details = userRepository.findByUsername("staff_1").get();

        boolean passwordMatches = passwordEncoder.matches("password", details.getPassword());
        log.info("THE RESULT IS >>> {}", passwordMatches);

        return userService.getUsers();
    }

}
