package com.prodigy.business.project.domain;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

/**
 * Repository for {@link Project} entity.
 */
public interface ProjectRepository extends JpaRepository<Project, UUID> {

}
