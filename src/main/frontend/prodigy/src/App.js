// This must be the first line in App.js
import 'react-app-polyfill/ie11';
import 'babel-polyfill';

import React, {Component} from 'react';
import {BrowserRouter, Route} from 'react-router-dom';
import './App.css';

import LoadingSpinner from "./components/common/LoadingSpinner";

import ManageEvents from "./components/event-management/ManageEvents";
import ManageUsers from "./components/user-management/ManageUsers";
import {UserData} from "./components/user-management/UserData";
import {getLoggedUserDetails} from "./components/fetch/user-fetch";

class App extends Component {

    constructor(props) {
        super(props);

        this.state = {
            userDataLoaded: false,
        }
    }

    componentWillMount() {
        getLoggedUserDetails()
            .then((userDetails) => {
                UserData.setUserValues(userDetails);
                this.setState({userDataLoaded: true});
            });
    }


    render() {
        if (!this.state.userDataLoaded) {
            return <LoadingSpinner/>;
        }

        return (
            <BrowserRouter>
                <div>
                    <Route exact path="/index.html" component={ManageEvents}/>
                    <Route exact path="/nav/users" component={ManageUsers}/>
                </div>
            </BrowserRouter>
        );
    }

}

export default App;
