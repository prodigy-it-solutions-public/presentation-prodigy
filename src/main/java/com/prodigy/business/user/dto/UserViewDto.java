package com.prodigy.business.user.dto;

import com.prodigy.business.user.domain.UserRole;
import lombok.*;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class UserViewDto extends UserBaseDto {

    private Long id;

    @Builder(builderMethodName = "viewBuilder")
    public UserViewDto(Long id, String username, String details, UserRole userRole) {
        super(username, details, userRole);
        this.id = id;
    }

}
