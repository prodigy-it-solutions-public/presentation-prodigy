package com.prodigy.business.project.domain;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.UUID;

@Data
@Entity
public class Project {

    @Id
    @GeneratedValue
    private UUID id;

    @Column(nullable = false)
    private String name;

    @Column(columnDefinition = "INT8 CONSTRAINT valid_duration CHECK (duration >= 1)")
    private long duration;

    // 9_999_999_999.99 (max value)
    @Column(name = "estimated_budget", nullable = false, columnDefinition = "numeric(12, 2) DEFAULT 0.00")
    private double estimatedBudget;

    @CreationTimestamp
    @Column(nullable = false, updatable = false)
    private OffsetDateTime created;

    @UpdateTimestamp
    @Column(insertable = false)
    private OffsetDateTime updated;

}
