package com.prodigy.business.project.impl;

import com.prodigy.business.project.ProjectService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

/**
 * Service method for {@link ProjectService} implementation.
 */
@Log4j2
@Service
public class ProjectServiceImpl implements ProjectService {

}
