package com.prodigy.business.task.domain;

import com.prodigy.business.project.domain.Project;
import com.prodigy.business.user.domain.User;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Data
@Entity
public class Task {

    // @Column here makes the entity persist. If @JoinColumn(name="id") is put on User, the save fails
    @Id
    @Column(name = "user_entity_id")
    private Long id;

    @Column(length = 5000)
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project_id")
    private Project project;

    // @MapsId makes this table use the same primary key as the UserDetails entity, making the OneToOne tight between
    // these two entities. Also, this is the most efficient relationship
    @OneToOne(fetch = FetchType.EAGER)
    @MapsId
    private User user;

    @CreationTimestamp
    @Column(nullable = false, updatable = false)
    private OffsetDateTime created;

    @UpdateTimestamp
    @Column(insertable = false)
    private OffsetDateTime updated;

}
