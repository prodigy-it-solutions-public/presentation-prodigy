package com.prodigy.business.user;

import com.prodigy.business.user.dto.UserInsertDto;
import com.prodigy.business.user.dto.UserUpdateDto;
import com.prodigy.business.user.dto.UserViewDto;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * User Service interface.
 */
@Validated
public interface UserService {

    /**
     * Get all the users from the database.
     *
     * @return list of users
     */
    @Transactional(readOnly = true)
    List<UserViewDto> getUsers();

    /**
     * Return the user which has the associated id.
     *
     * @param userId user identifier
     * @return the user
     */
    @Transactional(readOnly = true)
    UserViewDto getUser(@NotNull Long userId);

    /**
     * Insert a new user in the database.
     *
     * @param userInsertDto user insert dto
     * @return the inserted user
     */
    @Transactional(rollbackFor = Exception.class)
    UserViewDto addUser(@Valid UserInsertDto userInsertDto);

    /**
     * Update a user in the database.
     *
     * @param userUpdateDto user update dto
     * @return the updated user
     */
    @Transactional(rollbackFor = Exception.class)
    UserViewDto updateUser(@Valid UserUpdateDto userUpdateDto);

    /**
     * De-activate a user in the database.
     *
     * @param userId user identifier
     */
    @Transactional(rollbackFor = Exception.class)
    void deactivateUser(@NotNull Long userId);

}
