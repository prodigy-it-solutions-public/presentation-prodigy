create sequence hibernate_sequence;

create table user_entity
(
  id                      int8         not null,
  account_non_expired     boolean      not null,
  account_non_locked      boolean      not null,
  created                 timestamp    not null,
  credentials_non_expired boolean      not null,
  details                 varchar(255) not null,
  enabled                 boolean      not null,
  password                varchar(255) not null,
  role                    varchar(30)  not null,
  updated                 timestamp,
  username                varchar(255) not null,
  primary key (id)
);

alter table if exists user_entity
  drop constraint if exists UK_2jsk4eakd0rmvybo409wgwxuw;

-- password for these users inserted below is "password".
INSERT INTO user_entity (id, username, details, role, password, enabled, account_non_expired, account_non_locked, credentials_non_expired, created)
VALUES
(NEXTVAL('hibernate_sequence'), 'admin', 'The Admin', 'ADMIN', '$2a$10$ncDvZ9Wmyo3bGUIWBTvcD.XfzeeEc6lZ8HUTRUH6Cl5s0ncEvVsxa', true, true, true, true, NOW()),
(NEXTVAL('hibernate_sequence'), 'staff_1', 'Staff member 1', 'STAFF', '$2a$10$CuIfQjzAzBG3JHG2ofTuzutNCYpNOmvWss/CauLrjwcuAkHRyWrTO', true, true, true, true, NOW());