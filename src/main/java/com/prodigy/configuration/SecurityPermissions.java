package com.prodigy.configuration;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.stereotype.Component;

import static org.springframework.security.config.http.SessionCreationPolicy.IF_REQUIRED;

@Component
public class SecurityPermissions extends WebSecurityConfigurerAdapter {

    /**
     * Endpoints which we need to completely bypass the Spring Security filter chain.
     */
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                .antMatchers("/static/**",  "/style/**",
                        "/error");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .sessionManagement()
                    .maximumSessions(-1)
                    .expiredUrl("/login")
                    .and()
                    .sessionCreationPolicy(IF_REQUIRED)
                    .and()
                .authorizeRequests()
                    .antMatchers("/actuator/**",
                        "/auth/perform-login",
                        "/logout").permitAll()
                    .antMatchers("/management/**").hasAuthority("ADMIN")
                    .antMatchers("/api/**").permitAll()
                    .anyRequest().authenticated()
                    .and()
                .formLogin()
                    .loginPage("/login")
                    .loginProcessingUrl("/auth/perform-login")
                    .defaultSuccessUrl("/index.html", true)
                    .permitAll();
    }

}
