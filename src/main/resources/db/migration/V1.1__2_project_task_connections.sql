create table project (
  id               uuid                        not null,
  created          timestamp                   not null,
  duration         INT8 CONSTRAINT valid_duration CHECK (duration >= 1),
  estimated_budget numeric(12, 2) DEFAULT 0.00 not null,
  name             varchar(255)                not null,
  updated          timestamp,
  primary key (id)
);

create table task (
  created     timestamp not null,
  description varchar(5000),
  updated     timestamp,
  user_id     int8      not null,
  project_id  uuid,
  primary key (user_id)
);

create table user_to_project (
  id             uuid      not null,
  created        timestamp not null,
  updated        timestamp,
  project_id     uuid      not null,
  user_entity_id int8      not null,
  primary key (id)
);

alter table if exists user_to_project
  drop constraint if exists idx_project_to_user;
alter table if exists user_to_project
  add constraint idx_project_to_user unique (project_id, user_entity_id);

alter table if exists task
  add constraint FKk8qrwowg31kx7hp93sru1pdqa foreign key (project_id) references project;
alter table if exists task
  add constraint FKksqmgrkmwo22csb64ri6jt1fi foreign key (user_id) references user_entity;

alter table if exists user_to_project
  add constraint FK6qowrfeo1p6xkjey17x6p5649 foreign key (project_id) references project;
alter table if exists user_to_project
  add constraint FK3x6kyk1u6agvmd1aonvxfwfel foreign key (user_entity_id) references user_entity;
