import {doGet, doPost} from "./fetch-wrapper";

export function getLoggedUserDetails() {
    return doGet('/me/details');
}

export function logout() {
    return doPost('/logout', {});
}

export function getUsers() {
    return doGet('/management/users');
}
