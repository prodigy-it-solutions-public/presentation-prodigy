package com.prodigy.business.user.dto;

import com.prodigy.business.user.domain.UserRole;
import lombok.*;

/**
 * Dto used only to inserting a new user.
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class UserInsertDto extends UserBaseDto {

    @Builder(builderMethodName = "insertBuilder")
    public UserInsertDto(String username, String details, UserRole userRole) {
        super(username, details, userRole);
    }

}
