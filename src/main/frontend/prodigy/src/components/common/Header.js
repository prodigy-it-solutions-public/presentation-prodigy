import React from 'react';
import Logout from "./Logout";
import ManageUsersButton from "../user-management/ManageUsersButton";
import {UserData} from "../user-management/UserData";
import {UserPermissions} from "../user-management/UserPermissions";
import RenderIf from "./RenderIf";

export default function Header() {

    const userRole = UserData.getCurrentRole();

    return (
        <header className="navbar">
            <a className="navbar-logo" href="/index.html">
                <img src="/static/prodigy.jpg" alt="GHI Home"/>
            </a>
            <div>
                <span style={{'margin-right': '10px'}}>Hello {userRole.replace(/_/g, ' ')}</span>
                <RenderIf if={UserPermissions.canManageUsers}>
                    <ManageUsersButton/>
                </RenderIf>
                <Logout/>
            </div>
        </header>
    );
}