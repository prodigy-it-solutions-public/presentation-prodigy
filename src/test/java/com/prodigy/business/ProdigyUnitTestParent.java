package com.prodigy.business;

import com.prodigy.configuration.ApplicationConfiguration;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

/**
 * Base configuration (parent) class for JUnit 5, for Unit tests.
 */
@ExtendWith(MockitoExtension.class)
public abstract class ProdigyUnitTestParent {

    protected static final ModelMapper MODEL_MAPPER = new ApplicationConfiguration().modelMapper();

}
