package com.prodigy.business.user.impl;

import com.prodigy.business.ProdigyUnitTestParent;
import com.prodigy.business.user.UserService;
import com.prodigy.business.user.domain.User;
import com.prodigy.business.user.domain.UserRepository;
import com.prodigy.business.user.dto.UserViewDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.List;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

/**
 * Unit tests for {@link UserServiceImpl}.
 */
class UserServiceImplTest extends ProdigyUnitTestParent {

    @Mock
    private UserRepository userRepository;

    private UserService userService;

    @BeforeEach
    void setup() {
        userService = new UserServiceImpl(userRepository, MODEL_MAPPER);
    }

    @Test
    void should_GetAllUsers_ForValidRequest() {

        // GIVEN
        final User user = User.builder()
                .username("testUser")
                .build();

        when(userRepository.findAll()).thenReturn(singletonList(user));

        // WHEN
        final List<UserViewDto> users = userService.getUsers();

        // THEN
        assertNotNull(users);
        assertThat(users.size()).isEqualTo(1);

        final UserViewDto userViewDto = users.get(0);

        assertEquals("testUser", userViewDto.getUsername());
    }

}
