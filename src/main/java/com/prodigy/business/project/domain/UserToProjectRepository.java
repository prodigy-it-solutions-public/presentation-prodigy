package com.prodigy.business.project.domain;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

/**
 * Repository for {@link UserToProject} entity.
 */
public interface UserToProjectRepository extends JpaRepository<UserToProject, UUID> {

}
