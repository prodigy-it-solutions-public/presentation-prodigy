import fetch from 'isomorphic-fetch';

const loginUrl = window.location.origin + '/login';

// Added so application will work on edge - for more details check (the set-cookie header on response issued an extra wrong request for edge)
// https://stackoverflow.com/questions/46288437/set-cookie-header-has-no-effect-about-using-cookies-cors-including-for-local
function edgeFix(requestBody) {
    return Object.assign(requestBody, {credentials: 'include'});
}

function handleResponseErrors(response) {
    if (response.redirected && loginUrl === response.url) {
        window.location.assign("/");
        window.location.reload();

    } else {
        if (!response.ok) {
            // if user not authorised to perform an async request we will simply get him to the homepage
            throw Error(response.statusText);
        }
        return response.json();
    }
}

function doFetch(url, body) {
    return fetch(url, edgeFix(body))
        .then(handleResponseErrors);
}

export function doGet(url) {
    return doFetch(url, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    });
}

export function doPost(url, data) {
    return doFetch(url, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }

    });
}

