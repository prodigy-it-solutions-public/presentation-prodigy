import React, {Component} from 'react';

class LoadingSpinner extends Component {

    render() {
        return (
            <div className="loading">
                <img src="/static/loader.gif" alt="Loader"/>
            </div>
        );
    }
}

export default LoadingSpinner;