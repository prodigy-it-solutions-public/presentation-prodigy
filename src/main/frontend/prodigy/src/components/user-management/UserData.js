class UserDataClass {

    constructor() {
        this.defaults = {
            id: '#',
            role: 'GUEST',
            username: '#',
            details: '#'
        };

        this.userDetails = Object.assign({}, this.defaults);
    }

    resetToDefaults() {
        this.setUserValues(this.defaults);
    }

    static overrideProp(src, dest, prop) {
        dest[prop] = src[prop] || dest[prop];
    }

    setUserValues(data) {
        UserDataClass.overrideProp(data, this.userDetails, 'id');
        UserDataClass.overrideProp(data, this.userDetails, 'role');
        UserDataClass.overrideProp(data, this.userDetails, 'username');
        UserDataClass.overrideProp(data, this.userDetails, 'details');
    }

    getUserDetails() {
        return Object.assign({}, this.userDetails);
    }

    getCurrentRole() {
        return this.userDetails.role;
    }

}

export const UserData = new UserDataClass();