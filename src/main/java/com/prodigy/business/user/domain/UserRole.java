package com.prodigy.business.user.domain;

public enum UserRole {

    ADMIN,
    STAFF

}
