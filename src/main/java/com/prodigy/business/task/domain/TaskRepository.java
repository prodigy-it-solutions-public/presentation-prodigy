package com.prodigy.business.task.domain;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

/**
 * Repository for {@link Task} entity.
 */
public interface TaskRepository extends JpaRepository<Task, UUID> {

}
