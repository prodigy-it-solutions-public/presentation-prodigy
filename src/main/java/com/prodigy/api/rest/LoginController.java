package com.prodigy.api.rest;

import com.prodigy.business.user.dto.UserViewDto;
import com.prodigy.security.LoginService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("me")
public class LoginController {

    private final LoginService loginService;

    public LoginController(LoginService loginService) {
        this.loginService = loginService;
    }

    @GetMapping("details")
    public UserViewDto getUserDetails() {
        return loginService.getLoggedInUser();
    }

}
