# Jira

Jira Copycat - Project management application

# Specifications

- FE / BE separation
- Simple FE presentation: Login (admin / password), Manage Users Display, Logout
- WebMVC default mapping
- Security configurations and permissions
- Basic Architecture (Controller / Interface / Implementation / Repository)
- JSON mapping
- CRUD on UserController
- UserService interface with Javadocs and validations
- Basic View / Insert / Update User DTOs
- Simple rendering of UserServiceImpl details (high level)
- Project, Task, User entities (Project can have multiple Users, Users can have multiple 
Projects, Project can have multiple Tasks, User can have a single Task)
- Test structure (1 JUnit and 1 IT test) with Parent class
- Property files for local dev, develop and test environments
- Flyway migration scripts (Postgres Database)
- Pom structure with minimum requirements
- Latest versions of all libraries with explanations

# Usage
- mvn clean install
- localhost:8090
