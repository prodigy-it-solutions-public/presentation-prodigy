package com.prodigy.integration;

import com.prodigy.business.user.domain.UserRepository;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

/**
 * Base configuration (parent) class for JUnit 5, for IT.
 */
@Log4j2
@ActiveProfiles(profiles = {"test"})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class ProdigyITParent {

    @Autowired
    private TransactionTemplate transactionTemplate;

    private static UserRepository userRepository;

    @Autowired
    public void setUserLoginRepository(UserRepository userRepository) {
        ProdigyITParent.userRepository = userRepository;
    }

    private static boolean isSetupComplete = false;

    /**
     * Before each is called before each method.
     */
    @BeforeEach
    protected void setUp() {

        if (!isSetupComplete) {

            // do some magic here...
        }

        isSetupComplete = true;
    }

    /**
     * To avoid lazy initialization exceptions, we need to execute code in a transaction. This method exposes a transaction for this purpose, for integration
     * tests.
     */
    protected void testInTransaction(Runnable runnable) {
        transactionTemplate.execute(new TransactionCallbackWithoutResult() {

            @Override
            protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
                runnable.run();
            }
        });
    }

    /**
     * After all is called after each class.
     */
    @AfterAll
    public static void tearDown() {

        // do some magic here...

        isSetupComplete = false;
    }

}
