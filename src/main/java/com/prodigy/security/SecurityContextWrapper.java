package com.prodigy.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import static java.util.Optional.ofNullable;

public class SecurityContextWrapper {

    /**
     * Returns the current {@link UserDetails} entity which describes the current logged-in user.
     *
     * @return the logged-in user.
     * @throws AuthenticationException if there is no user logged in
     */
    public static UserDetails getCurrentLoggedInUser() throws AuthenticationException {
        return (UserDetails) ofNullable(SecurityContextHolder.getContext().getAuthentication())
                .map(Authentication::getPrincipal)
                .orElseThrow(() -> new AuthenticationException("Authentication not found") {
                });
    }

}
