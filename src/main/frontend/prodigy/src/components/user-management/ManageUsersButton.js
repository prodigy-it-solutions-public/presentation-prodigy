import React from 'react';
import {Link} from "react-router-dom";

export default function ManageUsersButton(props) {
    return <Link to='/nav/users' className="btn header-btn">Manage users</Link>;
}
