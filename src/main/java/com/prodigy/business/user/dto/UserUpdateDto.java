package com.prodigy.business.user.dto;

import com.prodigy.business.user.domain.UserRole;
import lombok.*;

/**
 * Dto used only to update an existing user with new information.
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class UserUpdateDto extends UserViewDto {

    @Builder(builderMethodName = "updateBuilder")
    public UserUpdateDto(Long id, String username, String details, UserRole userRole) {
        super(id, username, details, userRole);
    }

}
