package com.prodigy.configuration;

import org.springframework.boot.autoconfigure.web.servlet.error.ErrorViewResolver;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Component
public class WebMvcErrorResolver implements ErrorViewResolver {

    @Override
    public ModelAndView resolveErrorView(HttpServletRequest request, HttpStatus status, Map<String, Object> model) {

        if (status == HttpStatus.NOT_FOUND || status == HttpStatus.FORBIDDEN) {
            String path = (String) model.get("path");
            if (path != null && path.equals("/")) {
                return sendToHomepage();

            } else {
                return showPageNotFound();
            }
        }

        return null;
    }

    private ModelAndView showPageNotFound() {
        return new ModelAndView("forward:/error-page.html");
    }

    private ModelAndView sendToHomepage() {
        return new ModelAndView("redirect:/index.html");
    }
}
