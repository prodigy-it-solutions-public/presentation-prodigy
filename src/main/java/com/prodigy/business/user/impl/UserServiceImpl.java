package com.prodigy.business.user.impl;

import com.prodigy.business.user.UserService;
import com.prodigy.business.user.domain.UserRepository;
import com.prodigy.business.user.dto.UserInsertDto;
import com.prodigy.business.user.dto.UserUpdateDto;
import com.prodigy.business.user.dto.UserViewDto;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Service method for {@link UserService} implementation.
 */
@Log4j2
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final ModelMapper modelMapper;

    public UserServiceImpl(UserRepository userRepository, ModelMapper modelMapper) {
        this.userRepository = userRepository;
        this.modelMapper = modelMapper;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UserViewDto> getUsers() {

        log.info("Method={getUsers}, Details=[message={}]", "Get all available users");

        return userRepository.findAll().stream()
                .map(user -> modelMapper.map(user, UserViewDto.class))
                .collect(Collectors.toList());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserViewDto getUser(Long userId) {

        log.info("Method={getUser}, Details=[userId={}, message={}]", userId, "Get an existing user");

        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserViewDto addUser(UserInsertDto userInsertDto) {

        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserViewDto updateUser(UserUpdateDto userUpdateDto) {

        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deactivateUser(Long userId) {

    }

}
