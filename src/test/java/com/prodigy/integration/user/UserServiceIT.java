package com.prodigy.integration.user;

import com.prodigy.business.user.UserService;
import com.prodigy.business.user.dto.UserViewDto;
import com.prodigy.integration.ProdigyITParent;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Integration tests for {@link UserService} service.
 */
class UserServiceIT extends ProdigyITParent {

    @Autowired
    private UserService userService;

    // #################### getUsers ###############

    @DisplayName("Get users")
    @Test
    void should_GetAllUsers_ForValidRequest() {

        // GIVEN

        // WHEN
        final List<UserViewDto> users = userService.getUsers();

        // THEN
        assertThat(users).isNotNull();
        assertThat(users.size()).isGreaterThanOrEqualTo(2);

        users.forEach(userDto -> {
            assertNotNull(userDto);
            assertFalse(StringUtils.isEmpty(userDto.getUsername()));
        });
    }

}
