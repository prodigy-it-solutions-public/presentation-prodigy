import React, {Fragment} from "react";
import PropTypes from 'prop-types';

export default function RenderIf(props) {
    return (
        <Fragment>
            {props.if() ? props.children : props.orElse ? props.orElse : <Fragment/>}
        </Fragment>
    );
}

RenderIf.propTypes = {
    if: PropTypes.func.isRequired,
    orElse: PropTypes.element
};