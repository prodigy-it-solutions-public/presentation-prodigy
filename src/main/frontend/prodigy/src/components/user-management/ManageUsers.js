import React, {Component} from 'react';
import {withRouter} from "react-router-dom";

import Header from "../common/Header";
import {getUsers} from "../fetch/user-fetch";

class ManageUsers extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.getUsers();
    }

    getUsers = () => {
        getUsers().then((users) => {
            console.log('USERS ARE >> ' + JSON.stringify(users));
        })
    };

    render() {
        return (
            <article>
                <Header/>
                <main>
                    <h1>Here we will manage the users</h1>
                </main>
            </article>
        );
    }
}

export default withRouter(ManageUsers);