package com.prodigy.api.rest;

import com.prodigy.api.annotations.JsonRequestMapping;
import com.prodigy.business.user.UserService;
import com.prodigy.business.user.dto.UserInsertDto;
import com.prodigy.business.user.dto.UserUpdateDto;
import com.prodigy.business.user.dto.UserViewDto;
import com.prodigy.business.user.impl.UserServiceImpl;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping("management")
@PreAuthorize("hasAnyAuthority('ADMIN')")
public class UserController {

    private final UserService userService;

    public UserController(UserServiceImpl managementService) {
        this.userService = managementService;
    }

    @GetMapping("/users")
    List<UserViewDto> getUsers() {
        return userService.getUsers();
    }

    @JsonRequestMapping(value = "/{userId}", method = GET)
    public UserViewDto get(@PathVariable Long userId) {

        return userService.getUser(userId);
    }

    @JsonRequestMapping(method = POST)
    public UserViewDto add(@RequestBody UserInsertDto userInsertDto) {

        return userService.addUser(userInsertDto);
    }

    @JsonRequestMapping(method = PUT)
    public UserViewDto update(@RequestBody UserUpdateDto userUpdateDto) {

        return userService.updateUser(userUpdateDto);
    }

    @JsonRequestMapping(value = "/{userId}", method = DELETE)
    public void remove(@PathVariable Long userId) {

        userService.deactivateUser(userId);
    }

}
