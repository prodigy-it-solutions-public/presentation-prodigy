import React, {Component} from 'react';
import {withRouter} from "react-router-dom";

import Header from "../common/Header";

class ManageEvents extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.getEvents();
    }

    getEvents = () => {

    };

    render() {
        return (
            <article>
                <Header/>
                <main>
                    <h1>Here we will manage the events</h1>
                </main>
            </article>
        );
    }
}

export default withRouter(ManageEvents);