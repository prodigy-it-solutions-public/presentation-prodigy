package com.prodigy.security;

import com.prodigy.business.user.domain.UserRepository;
import com.prodigy.business.user.dto.UserViewDto;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class LoginService implements UserDetailsService {

    private final UserRepository userRepository;
    private final ModelMapper modelMapper;

    public LoginService(UserRepository userRepository, ModelMapper modelMapper) {
        this.userRepository = userRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("Couldn't find an user with the username " + username));
    }

    @Transactional(readOnly = true)
    public UserViewDto getLoggedInUser() {
        String loggedUser = SecurityContextWrapper.getCurrentLoggedInUser().getUsername();
        UserDetails userDetails = loadUserByUsername(loggedUser);
        return modelMapper.map(userDetails, UserViewDto.class);
    }

}
