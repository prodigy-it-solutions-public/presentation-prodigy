package com.prodigy.business.task.impl;

import com.prodigy.business.task.TaskService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

/**
 * Service method for {@link TaskService} implementation.
 */
@Log4j2
@Service
public class TaskServiceImpl implements TaskService {

}
