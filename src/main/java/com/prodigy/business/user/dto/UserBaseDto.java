package com.prodigy.business.user.dto;

import com.prodigy.business.user.domain.UserRole;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Base User DTO.
 */
@Data
@Builder(builderMethodName = "basicBuilder")
@NoArgsConstructor
@AllArgsConstructor
class UserBaseDto {

    @NotBlank
    private String username;

    @NotBlank
    private String details;

    @NotNull
    private UserRole role;

}
