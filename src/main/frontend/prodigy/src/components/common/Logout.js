import React, {Component} from 'react';
import {UserData} from "../user-management/UserData";
import {logout} from "../fetch/user-fetch";

class Logout extends Component {

    handleLogout(e) {
        logout().then(() => {
            UserData.resetToDefaults();
        });
    }

    render() {
        return <a href="#" className="btn header-btn logout-btn" onClick={(e) => this.handleLogout(e)}>
            <span>Logout</span>
        </a>;
    }
}

export default Logout;
